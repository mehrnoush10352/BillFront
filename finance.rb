require 'json'
class Finance

  def get_credit_line
    file = File.read('credit_line.json')
    credit = JSON.parse(file)
    
  end

  def get_invoice_list
    file = File.read('invoices.json')
    JSON.parse(file)
  end

  def financed
    credit_line = get_credit_line 
    
    enable_money = credit_line['customer']
    invoices = get_invoice_list
    invoices.each_with_index do |invoice, index|
      next if enable_money < invoice['amount']
      next if invoice['is_paid']
     enable_money -= invoice['amount']
     invoices[index]['is_paid'] = true
    end
   update_files('invoices.json', invoices) 
   credit_line['customer'] = enable_money
   update_files('credit_line.json', credit_line)
  end

  def update_files(file, data)
    File.open(file,"w") do |f|
      f.write(JSON.pretty_generate(data))
    end
  end
end
Finance.new.financed
